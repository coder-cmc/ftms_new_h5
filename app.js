
let config = {};
if (process.env.NODE_ENV == 'production'||true) { //生产环境
    config = {
        // ajax地址
        baseUrl: 'https://admin.bxtcoins.com',
        // 图片地址 （暂时无用）
        imgUrl: 'https://admin.bxtcoins.com/storage',
        // socket地址
        socketUrl: 'wss://admin.bxtcoins.com/ws1',
        socketUrl1: 'wss://admin.bxtcoins.com/ws2',
        // pc端地址
        pcUrl:'https://pc.fxtmsolutions.com',
        // app名称
        appName: 'BXTCOIN',
        // 版本
        version: '1.0.0',
        // 移动端地址
        mobile: 'https://m.fxtmsolutions.com'
    };
} else { //开发环境
    config = {
        baseUrl: 'https://admin.bxtcoins.com',
        imgUrl: 'https://admin.bxtcoins.com/upload',
        socketUrl: 'ws://admin.bxtcoins.com:2346/',
        socketUrl1: 'ws://admin.bxtcoins.com:2348/',
        // pc端地址
        pcUrl:'https://m.fxtmsolutions.com',
        // app名称
        appName: '本地开发',
        // 版本
        version: '0.0.0',
        // 移动端地址
        mobile: ''
    }
    // config = {
    //     // ajax地址
    //     baseUrl: 'https://server.7coin.in',
    //     // 图片地址 （暂时无用）
    //     imgUrl: 'https://server.7coin.in/upload',
    //     // socket地址
    //     socketUrl: 'wss://server.7coin.in:2346/',
    //     socketUrl1: 'wss://server.7coin.in:2348/',
    //     // pc端地址
    //     pcUrl:'https://www.7coin.in',
    //     // app名称
    //     appName: '7COIN test',
    //     // 版本
    //     version: '1.0.0',
    //     // 移动端地址
    //     mobile: 'https://h5.7coin.in'
    // };
}
export default config;